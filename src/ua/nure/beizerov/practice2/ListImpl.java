package ua.nure.beizerov.practice2;


import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * This class is represent a List implementation.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class ListImpl implements List {
	
	private Node head;
	private Node tail;
	private int listSize;

	
	public static void main(String[] args) {
		List list = new ListImpl();
		
		list.addFirst("A");
		list.addFirst("B");
		list.addFirst("C");
		
		System.out.println(list);
		System.out.println(list.size());
		
		
		list.remove("B");
		list.remove("A");
		list.remove("C");

		System.out.println(list);
		System.out.println(list.size());
	}
	
	
	@Override
	public void clear() {
		head = null;
		tail = null;
		listSize = 0;
	}

	@Override
	public int size() {
		return listSize;
	}

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	@Override
	public void addFirst(Object element) {
		Node node = new Node(element);
		
		if (head == null) {
			tail = head = node;
		} else {
			Node nodeReference = head;
			
			head.previous = node;
			head = node;
			head.next = nodeReference;
		}
		
		listSize++;
	}

	@Override
	public void addLast(Object element) {
		Node node = new Node(element);
		
		if (head == null) {
			tail = head = node;
		} else {
			Node nodeReference = tail;
			
			tail.next = node;
			tail = node;
			tail.previous = nodeReference;
		}
		
		listSize++;
	}

	@Override
	public void removeFirst() {
		if (head != null) {
			if(head.next != null) {
				head = head.next;
				head.previous = null;
			} else {
				head = tail = null;
			}
			
			listSize--;
		}
	}

	@Override
	public void removeLast() {
		if (tail != null) {
			if (tail.previous != null) {
				tail = tail.previous;
				tail.next = null;
			} else {
				tail = head = null;
			}
			
			listSize--;
		}
	}

	@Override
	public Object getFirst() {
		return head.data;
	}

	@Override
	public Object getLast() {
		return tail.data;
	}

	@Override
	public Object search(Object element) {
		
		IteratorImpl impl = new IteratorImpl();
		
		while (impl.hasNext()) {
			Object obj = impl.next();
			
			if (element.equals(obj)) {
				return obj;
			}
		}
		return null;
	}
	
	private Node findNode(Object element) {
		Node nodeReference = head;
		
		if (nodeReference != null) {
			while (nodeReference != null) {
				if (nodeReference.data.equals(element)) {
					break;
				}
				
				nodeReference = nodeReference.next;
			}
		}
		
		return nodeReference;
	}

	@Override
	public boolean remove(Object element) {	
		Node ref = findNode(element);

		if (ref == null) {
			return false;
		}
		
		if (ref.previous != null) {
			ref.previous.next = ref.next;
			
			if (ref.next != null) {
				ref.next.previous = ref.previous;
			} else {
				removeLast();
				
				return true;
			}
		} else {
			removeFirst();
			
			return true;
		}
		
		listSize--;
		
		return true;
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		
		output.append("[");
		
		
		Node nodeReference = head;
		
		
		if (nodeReference != null) {
			while (nodeReference != null) {
				output.append(nodeReference);
				
				if (nodeReference.next != null) {
					output.append(", ");
				}
				
				nodeReference = nodeReference.next;
			}
		}
		
		output.append("]");
		
		return output.toString();
	}
	
	
	private static final class Node {
		
		private Object data;
		private Node next;
		private Node previous;
		
		
		Node(Object data) {
			this.data = data;
			this.next = null;
			this.previous = null;
		}
		
		
		@Override
		public String toString() {
			return  data.toString();
		}	
	}
	
	
	private final class IteratorImpl implements Iterator<Object> {

		private Node current;
		private Node previousNode;
		private boolean isNext;
		
		
		IteratorImpl() {
			current = head;
		}

		@Override
		public boolean hasNext() {
			return current != null;
		}

		@Override
		public Object next() {
			if (current == null) {
				throw new NoSuchElementException();
			}
			
			Node node = current;
			previousNode = current;
			current = current.next;
			
			isNext = true;
			
			return node.data;
		}
		
		@Override
		public void remove() {
			if (!isNext) {
				throw new IllegalStateException();
			} else if (previousNode == head) {
				head = head.next;
			} else {
				previousNode.previous.next = previousNode.next;
			}
			
			isNext = false;
		}
	}
}
