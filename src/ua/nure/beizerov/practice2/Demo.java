// encoding = UTF8
package ua.nure.beizerov.practice2;


/**
 * This class is represent a demo.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Demo {

	public static void main(String[] args) {
		ArrayImpl.main(null);
		ListImpl.main(null);
		QueueImpl.main(null);
		StackImpl.main(null);
	}
}
