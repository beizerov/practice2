package ua.nure.beizerov.practice2;


import java.util.Iterator;


/**
 * This class is represent a Queue implementation.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class QueueImpl implements Queue {

	private final List list = new ListImpl();
		
	
	public static void main(String[] args) {
		Queue queue = new QueueImpl();

		System.out.println(queue);
	}
	

	@Override
	public void clear() {
		list.clear();
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public Iterator<Object> iterator() {
		return list.iterator();
	}

	@Override
	public void enqueue(Object element) {
		list.addLast(element);
	}

	@Override
	public Object dequeue() {
		Object data = list.getFirst();
		list.removeFirst();
		
		return data;
	}

	@Override
	public Object top() {
		return list.getFirst();
	}
	
	@Override
	public String toString() {
		return list.toString();
	}
}
