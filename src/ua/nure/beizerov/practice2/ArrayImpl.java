package ua.nure.beizerov.practice2;


import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * This class is represent a Array implementation.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class ArrayImpl implements Array {
	
	private static final double FIFTY_PERCENT = 0.5;
	
	private Object[] array;
	private int capacity;
	private int currentIndex = -1;

	
	public ArrayImpl() {
		this(10);
	}
	
	public ArrayImpl(int capacity) {
		this.capacity = capacity;
		this.array = new Object[capacity];
	}
	
	
	public static void main(String[] args) {
		ArrayImpl array = new ArrayImpl();
		
		array.add("A");
		array.add("B");
		array.add("C");

		System.out.println(array.indexOf("A"));
		System.out.println(array.indexOf("B"));
		System.out.println(array.indexOf("C"));
		System.out.println(array.indexOf("D"));
	}


	@Override
	public void clear() {
		currentIndex = -1;
		capacity = 10;
		array = new Object[capacity];
	}

	@Override
	public int size() {
		return currentIndex + 1;
	}

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	@Override
	public void add(Object element) {
		if(currentIndex < capacity) {
			array[++currentIndex] = element;
		} else {
			capacity += capacity * FIFTY_PERCENT;
			Object[] newArray = new Object[capacity];
			
			for (int i = 0, length = array.length; i < length; i++) {
				newArray[i] = array[i];
			}
			
			array = newArray;
			
			array[++currentIndex] = element;
		}
	}

	@Override
	public void set(int index, Object element) {
		array[index] = element;
	}

	@Override
	public Object get(int index) {
		return array[index];
	}

	@Override
	public int indexOf(Object element) {
		for (int i = 0; i <= currentIndex; i++) {
			if(array[i].equals(element)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public void remove(int index) {
		Object[] newArray = new Object[array.length - 1];
		
		int j = 0;
		
		for (int i = 0, length = array.length; i < length; i++, j++) {
			if (i == index) {
				j--;
				continue;
			}
			
			newArray[j] = array[i];
		}
		
		currentIndex--;
		array = newArray;
	}
	
	@Override
	public String toString() {
		
		StringBuilder output = new StringBuilder();
		
		output.append("[");
		
		for (int i = 0; i <= currentIndex ;i++) {
			output.append(array[i]);
			
			if (i < currentIndex) {
				output.append(", ");
			}
		}
		
		output.append("]");
		
		return output.toString();
	}
	
	
	private final class IteratorImpl implements Iterator<Object> {
		
		private int index;
		private boolean isNext;

		@Override
		public boolean hasNext() {
			return index <= currentIndex;
		}

		@Override
		public Object next() {
			if (index > currentIndex) {
				throw new NoSuchElementException();
			}
			
			isNext = true;
			
			return array[index++];
		}
		
		@Override
		public void remove() {
			if (!isNext) {
				throw new IllegalStateException();
			}
			
			ArrayImpl.this.remove(index - 1);
			index--;
			isNext = false;
		}
	}
}
