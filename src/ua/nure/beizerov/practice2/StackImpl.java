package ua.nure.beizerov.practice2;


import java.util.Iterator;


public class StackImpl implements Stack {
	
	private final List list = new ListImpl();
	
	
	public static void main(String[] args) {
		Stack stack = new StackImpl();
		stack.push("A");
		stack.push("B");
		stack.push("C");
		
		System.out.println(stack);
		System.out.println(stack.size());
	}
	

	@Override
	public void clear() {
		list.clear();
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public Iterator<Object> iterator() {
		return list.iterator();
	}

	@Override
	public void push(Object element) {
		list.addFirst(element);
	}

	@Override
	public Object pop() {
		Object data = list.getFirst();
		list.removeFirst();
		
		return data;
	}

	@Override
	public Object top() {
		return list.getFirst();
	}
	
	@Override
	public String toString() {
		String str = list.toString();
		
		str = str.replaceAll(", ", " ,");
		str = str.substring(1, str.length() - 1);
		
		StringBuilder output = new StringBuilder();
		
		output
			.append('[')
			.append(new StringBuilder(str).reverse())
			.append(']')
		;
		
		return output.toString();
	}
}
